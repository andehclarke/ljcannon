<?php
/**
 * Displays archive pages if a speicifc template is not set. 
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>
			
	<div class="content">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
		
		    <main class="main small-12 medium-8 large-8 cell grid-x grid-margin-x" role="main" id="ljc-main">
			    
		    	<header class="cell">
					<?php the_breadcrumb(); ?>
		    		<h1 class="page-title"><?php single_term_title();?></h1>
					<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
		    	</header>
				<div id="ljc-stuck" class="grid-x grid-padding-x">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<div class="small-12 large-6 cell">
							<!-- To see additional archive styles, visit the /parts directory -->
							<?php get_template_part( 'parts/loop', 'archive' ); ?>
						</div>
						
					<?php endwhile; ?>
					
				</div>

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
		
			</main> <!-- end #main -->
	
			<?php get_sidebar(); ?>
	    
	    </div> <!-- end #inner-content -->
	    
	</div> <!-- end #content -->

<?php get_footer(); ?>