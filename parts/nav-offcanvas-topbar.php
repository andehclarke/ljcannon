<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar small-center" id="top-bar-menu">
	<div class="top-bar-left float-left">
		<ul class="menu">
			<li><a id="logo" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" /></a></li>
			<li class="show-for-small-only">
				<a data-toggle="off-canvas" class="ljc-menu-btn"><?php _e( 'Menu', 'jointswp' ); ?>
					<div class="ljc-hamburger">
						<div class="ljc-burger"></div>
						<div class="ljc-burger"></div>
						<div class="ljc-burger"></div>
					</div>
				</a>
			</li>
		</ul>
	</div>
	<div class="top-bar-right show-for-medium">
		<?php joints_top_nav(); ?>	
	</div>
</div>