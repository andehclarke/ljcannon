<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="off-canvas-fullscreen position-right" id="off-canvas" data-off-canvas data-transition="overlap" data-content-scroll="false">
	<div class="off-canvas-fullscreen-inner">
		<button class="off-canvas-fullscreen-close" aria-label="Close menu" type="button" data-close>
			<span aria-hidden="true">&times;</span>
		</button>
		<?php joints_off_canvas_nav(); ?>
	</div>
</div>

<div class="off-canvas-fullscreen position-right " id="off-canvas-quote" data-off-canvas data-transition="overlap" data-content-scroll="false">
	<div class="off-canvas-fullscreen-inner">
		<button class="off-canvas-fullscreen-close" aria-label="Close menu" type="button" data-close>
			<span aria-hidden="true">&times;</span>
		</button>
		<?php dynamic_sidebar( 'sidebar1' ); ?>
		<?php the_ljc_contact_form(); ?>

	</div>
</div>