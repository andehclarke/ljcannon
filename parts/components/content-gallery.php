<?php $gallery = get_field('portfolio_gallery'); ?>

<?php if($gallery) : ?>
    <?php foreach($gallery as $key => $image) : ?>
        <a data-open="galleryModal">
            <?php echo wp_get_attachment_image($image['id'], 'thumbnail'); ?>
        </a>
    <?php endforeach; ?>
    <div class="reveal" id="galleryModal" data-reveal>
        <div class="portfolio-gallery">
            <?php foreach($gallery as $key => $image) : ?>
                <div>
                    <?php echo wp_get_attachment_image($image['id'], 'full', array('class' => 'orbit-image')); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>