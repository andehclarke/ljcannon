<?php $limit = 4; ?>
<?php $gallery = get_field('portfolio_gallery'); ?>

<?php if($gallery) : ?>
    <div class="grid-x">
        <?php foreach($gallery as $key => $image) : ?>
            <?php if($key < $limit) : ?>
                <div class="cell small-6 medium-4 large-6 xlarge-4 ljc-portfolio-images">
                    <?php echo wp_get_attachment_image($image['id'], 'thumbnail', array('class' => 'orbit-image')); ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>