<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cell'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<?php the_breadcrumb(); ?>
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
		<?php // get_template_part( 'parts/content', 'byline' ); ?>
    </header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('full'); ?>
		<div class="ljc-gallery">
			<?php get_template_part( 'parts/components/content', 'gallery' ); ?>
		</div>
		<p class="tags"><?php the_tags('',''); ?></p>
		<?php the_content(); ?>
		<?php if(get_field('show_testimonial')) : ?>
			<?php $testimonial = get_field('testimonial'); ?>
			<?php $testimonial = $testimonial[0]; ?>
			<span class="h2">Customer Review</span>
			<?php echo do_shortcode('[single_testimonial id="'.$testimonial.'" theme="default_style" show_title="0" use_excerpt="0" show_thumbs="0" show_date="1" show_other="1" hide_view_more="0" output_schema_markup="1" show_rating="stars"]'); ?>
		<?php endif; ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
	</footer> <!-- end article footer -->
						
	<?php // comments_template(); ?>	
													
</article> <!-- end article -->