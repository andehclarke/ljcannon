<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cell category-border'); ?> role="article">					
	
	<header class="article-header">
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<?php //get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->
					
	<section class="entry-content" itemprop="articleBody">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?>
			<?php get_template_part( 'parts/components/archive', 'gallery' ); ?>
		</a>
		<p class="tags"><?php the_tags('',''); ?></p>
		<?php the_content('' . __( 'Read more...', 'jointswp' ) . ''); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
    	<!-- <p class="tags"><?php //the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p> -->
	</footer> <!-- end article footer -->	
				    						
</article> <!-- end article -->