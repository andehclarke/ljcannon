<?php 
/**
 * The sidebar containing the main widget area
 */
 ?>

<div class="show-for-small-only small-12 cell grid-x grid-margin-x" id="ljc-contact-hover" data-sticky-container>
	<div class="sticky cell text-right" data-sticky data-top-anchor="ljc-main" data-btm-anchor="ljc-footer:bottom" data-margin-top="1" data-sticky-on="small">
		<a data-toggle="off-canvas-quote">
			<button class="button">Get a quote</button>
		</a>
	</div>
</div>

<div id="sidebar1" class="sidebar small-12 medium-4 large-4 cell grid-x grid-margin-x show-for-medium" data-sticky-container role="complementary">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>
		
		<div class="sticky" data-sticky data-top-anchor="ljc-main" data-btm-anchor="ljc-footer" data-margin-top="1">

			<?php dynamic_sidebar( 'sidebar1' ); ?>
			<?php the_ljc_contact_form(); ?>

		</div>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
		<div>
			<h2>Get a quote</h2>
		</div>

	<?php endif; ?>

</div>