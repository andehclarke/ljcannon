<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo" id="ljc-footer">
					
					<div class="inner-footer grid-x grid-margin-x grid-padding-x">
						
						<div class="small-6 medium-4 large-4 cell">
							<nav role="navigation">
	    						<?php joints_footer_links(); ?>
	    					</nav>
						</div>
						
						<div id="social-links" class="cell small-6 medium-4 large-4 text-center">
							<?php
								$facebook_url = get_option('facebook_url');
								$twitter_url = get_option('twitter_url');
								$contact_email = get_option('contact_email');
								if($facebook_url != null) {
									echo '<a target="_blank" href="'.$facebook_url.'" class="socials"><span class="socicon-facebook"></span></a>';
								}
								if($twitter_url != null) {
									echo '<a target="_blank" href="'.$twitter_url.'" class="socials"><span class="socicon-twitter"></span></a>';
								}
								if($contact_email != null) {
									echo '<a target="_blank" href="mailto:'.$contact_email.'" class="socials"><span class="socicon-mail"></span></a>';
								}
							?>
						</div>
						
						<div class="small-12 medium-4 large-4 cell text-right">
							<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							<p class="credits">Designed by <a target="_BLANK" href="http://www.andyclarke.website">Andy Clarke</a></p>
						</div>
					
					</div> <!-- end #inner-footer -->
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->