<?php

// add custom class to tag
function add_class_the_tags($html){
    $postid = get_the_ID();
    $html = str_replace('<a','<a class="button"',$html);
    return $html;
}
add_filter('the_tags','add_class_the_tags');