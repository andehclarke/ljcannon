<?php

function get_ljc_contact_form() {
    $form = '<form data-abide novalidate action="" method="POST" id="ljc-quote-form">
        <div data-abide-error class="alert callout" style="display: none;">
            <p><i class="fi-alert"></i> There are some errors in your form.</p>
        </div>
        <div class="grid-x row">
            <div class="small-12 columns">
            <label>
                <h4>Name</h4>
                <input type="text" name="cf_name" placeholder="Your Name" required pattern="text" />
                <span class="form-error">
                    What\'s your name?
                </span>
            </label>
            </div>
            <div class="small-12 columns">
            <label>
                <h4>Email</h4>
                <input type="email" name="cf_email" placeholder="example@email.com" required pattern="email" />
                <span class="form-error">
                    What\'s your email?
                </span>
            </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
            <label>
                <h4>Details</h4>
                <textarea rows="5" name="cf_message" placeholder="Details of work you would like quoted"></textarea>
            </label>
            </div>
        </div>
        <div class="row submit">
            <input class="hide-me" name="anti-spam" type="text" />
            <button class="button large" type="submit" value="Send">Submit</button>
        </div>
    </form>';
    // return $form;
    return do_shortcode('[wpforms id="354"]');
}

function the_ljc_contact_form() {
    echo get_ljc_contact_form();
}