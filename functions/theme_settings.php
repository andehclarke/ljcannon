<?php

// Add custom theme settings page
function theme_settings_page(){
    ?>
    <div class="wrap">
        <h1>Theme Panel</h1>
        <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
    </div>
	<?php
}

// Add it to the admin menu
function add_theme_menu_item()
{
    add_menu_page("LJC Theme Settings", "LJC Theme Settings", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function display_twitter_element()
{
	?>
    	<input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
    <?php
}

function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}

function display_email_element()
{
	?>
    	<input type="email" name="contact_email" id="contact_email" value="<?php echo get_option('contact_email'); ?>" />
    <?php
}

function display_tagline_top_element()
{
	?>
    	<input type="text" name="tagline_top" id="tagline_top" value="<?php echo get_option('tagline_top'); ?>" />
    <?php
}

function display_tagline_bottom_element()
{
	?>
    	<input type="text" name="tagline_bottom" id="tagline_bottom" value="<?php echo get_option('tagline_bottom'); ?>" />
    <?php
}

// Add custom settings to view and register them
function display_theme_panel_fields()
{
    add_settings_section("section", "All Settings", null, "theme-options");
    add_settings_field("tagline_top", "Top Tagline", "display_tagline_top_element", "theme-options", "section");
	add_settings_field("tagline_bottom", "Bottom Tagline", "display_tagline_bottom_element", "theme-options", "section");
	add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("contact_email", "Contact Email", "display_email_element", "theme-options", "section");
    register_setting("section", "tagline_top");
    register_setting("section", "tagline_bottom");    
    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "contact_email");
}

add_action("admin_init", "display_theme_panel_fields");