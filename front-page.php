<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>
			
	<div class="content">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 medium-12 large-12 cell grid-x grid-margin-x" role="main" id="home-page">
		    
                <div class="small-12 medium-12 medium-order-3 large-4 large-order-2 cell text-center">
                    <h1>Builder &amp; brickwork specialist</h1>
                    <a href="/quote/" class="button large expanded">Get a quote</a>
                </div>
                
                <div class="small-12 medium-6 medium-order-1 large-4 large-order-1 cell">
                    <img class="imgleft" src="<?php echo get_template_directory_uri(); ?>/assets/images/rightimage.jpg" />
                </div>

                <div class="small-12 medium-6 medium-pull-6 medium-order-2 large-4 large-order-3 cell">
                    <img class="imgright" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner.jpeg" />
                </div>

                <div class="small-12 cell medium-order-4 text-center">
                    <span class="h2"><?php echo get_option('tagline_top');?></span>
                    <div class="page-break">
                        <span class="h6"><?php echo get_option('tagline_bottom');?></span>
                    </div>
                </div>

                <div class="small-12 medium-4 medium-order-4 cell">
                    <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/plans.svg" />
                    <h2 class="text-center">Planning</h2>
                    <p>Working with you right from the beginning to help during the planning stages and giving you the confidence to realise your dream.</p>
                    <p>Working with local architects I can help make that mental picture a reality.</p>
                </div>

                <div class="small-12 medium-4 medium-order-4 cell">
                    <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/wall3.svg" />
                    <h2 class="text-center">Building</h2>                    
                    <p>Using the best materials for the job alongside 10 years of knowledge and passion your project will really come to life.</p>
                    <p>We will provide you with a clear project plan that follows industry standard best practice and is flexible to work around your busy life.</p>
                </div>

                <div class="small-12 medium-4 medium-order-4 cell">
                    <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/castle.svg" />
                    <h2 class="text-center">Finished</h2>
                    <p>Working with experienced plasterers, painters and decorators I'm able to take your project right through to the finishing touches.</p>
                    <p>I always aim to leave somewhere better than I found it. Why not give me a call to discuss your dream?</p> 
                </div>

                <div id="services" class="small-12 medium-12 medium-order-4 cell grid-x text-center">
                    <div class="small-12">
                        <h2>Services</h2>
                        <p>A high quality and reliable builder covering Warwickshire & South Leicestershire, with years of experience in brickwork and building from large construction sites to small garden walls.  All work is considered and a free quote and consultation is included for all services.</p>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/extension/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/extension.svg" />
                        <h3>Home extensions</h3>
                        <span class="button tiny tags">Extension</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/new-build/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/roof.svg" />
                        <h3>New build homes</h3>
                        <span class="button tiny tags">New build</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/groundworks/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/dumper.svg" />
                        <h3>Groundworks</h3>
                        <span class="button tiny tags">Groundworks</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/loft-conversion/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/cabin.svg" />
                        <h3>Loft Conversions</h3>
                        <span class="button tiny tags">Loft Conversions</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/brickwork/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/wall-garden.svg" />
                        <h3>Brickwork</h3>
                        <span class="button tiny tags">Brickwork</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/block-paving/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/block-paving.svg" />
                        <h3>Block paving and slabbing</h3>
                        <span class="button tiny tags">Block paving</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/garage-conversion/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/garage.svg" />
                        <h3>Garage Conversions</h3>
                        <span class="button tiny tags">Garage Conversions</span></a>
                    </div>
                    <div class="small-6 medium-4 large-3">
                        <a href="/tag/carpentry/">
                        <img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/carpentry.svg" />
                        <h3>Carpentry</h3>
                        <span class="button tiny tags">Carpentry</span></a>
                    </div>
                </div>

                <div class="small-12 medium-order-4 cell">
                    <?php echo do_shortcode("[testimonials_cycle random='true' use_excerpt='1' timer='10000']"); ?>
                </div>
																								
		    </main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>